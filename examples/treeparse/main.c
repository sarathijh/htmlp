#include "../../include/htmlp.h"
#include <stdio.h>

int main (int argc, char **argv) {
  html_node *root = htmlp_treeparse (fopen (argv [1], "r"));
  if (argc >= 3) {
    array *nodes = html_node_query_selector_all (root, argv [2]);
    FOREACH(nodes, html_node*, node) {
      htmlp_pretty_print(node);
    }
  } else {
    htmlp_pretty_print (root);
  }
  html_node_free (root);
  return 0;
}
