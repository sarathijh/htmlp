#include "../../include/htmlp.h"
#include <stdio.h>

int indent = 0;

void handle_tag_open (char *name, array *attrs) {
  for (int i = 0; i < indent; ++i) { printf ("  "); }
    
  printf ("<\033[1;1m%s\033[0m", name);
  FOREACH(attrs, html_attr*, attr) {
    printf (" \033[1;1m%s\033[0m=\"\033[1;33m%s\033[0m\"", attr->name, attr->value);
  }
  printf (">\n");
  
  ++indent;
  
  free (name);
  array_free_f (attrs, FREE_FUNC(html_attr_free));
}

void handle_tag_close (char *name) {
  --indent;
  
  for (int i = 0; i < indent; ++i) { printf ("  "); }
  
  printf ("</\033[1;1m%s\033[0m>\n", name);
  
  free (name);
}

void handle_text (char *text) {
  if (text [0] == ' ' && text [1] == '\0') { free (text); return; }
  
  for (int i = 0; i < indent; ++i) { printf ("  "); }
  
  printf ("\033[1;31m%s\033[0m\n", text);
  
  free (text);
}

int main (int argc, char **argv) {
  htmlp_on_tag_open  = handle_tag_open;
  htmlp_on_tag_close = handle_tag_close;
  htmlp_on_text      = handle_text;
  
  htmlp_iterparse (fopen (argv [1], "r"));
  
  return 0;
}
