# README #

A simple incremental parser for HTML written in C.

Can currently parse opening tags and attributes, closing tags, and text nodes.

iterparse.c will parse an HTML file and send messages as parsing events occur.
A parsing event is either an opening tag has been parsed (including all its attributes),
a closing tag has been parsed, or a text node has been parsed.

treeparse.c will use iterparse.c to create a tree structure from a fully parsed HTML file.
This tree structure can be queried using CSS selector strings.

![Full Parsed Tree](examples/img/full-tree.png "Full Parsed Tree")
![Selectors 1](examples/img/selectors1.png "Selectors 1")
![Selectors 2](examples/img/selectors2.png "Selectors 2")
