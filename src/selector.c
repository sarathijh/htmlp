#include "selector.h"
#include "string.h"
#include <ctype.h>
#include <string.h>

int is_class_char (int c) {
  return isalpha (c) || isdigit (c) || c == '-' || c == '_';
}

html_selector* single_selector (char *str, int *pos) {
  html_selector *root = 0;
  int i = *pos;
  while (1) {
    while (isspace (str [i])) { ++i; }
    
    if (str [i] == '\0') { break; }
    
    html_selector *sel = (html_selector*)malloc (sizeof(html_selector));
    sel->attrs = array_alloc (1);
    sel->next = 0;
    
    if (isalpha (str [i])) {
      sel->type = SEL_TAG;
    } else if (str [i] == '.') {
      sel->type = SEL_CLASS;
      ++i;
    } else if (str [i] == '#') {
      sel->type = SEL_ID;
      ++i;
    }
    
    if (root == 0) {
      if (sel->type == SEL_TAG) {
        root = sel;
      } else {
        root = (html_selector*)malloc (sizeof(html_selector));
        root->type = SEL_TAG;
        root->pattern = 0;
        root->attrs = array_alloc (1);
        array_add (root->attrs, sel);
        root->next = 0;
      }
    } else {
      array_add (root->attrs, sel);
    }
    
    int l = i;
    while (str [i] != '.' && str [i] != '#' && !isspace (str [i]) && str [i] != '\0') {
      ++i;
    }
    sel->pattern = (char*)malloc (i-l+1);
    strncpy (sel->pattern, str+l, i-l);
    sel->pattern [i-l] = '\0';
    
    if (str [i] == '\0' || isspace (str [i])) { break; }
  }
  *pos = i;
  return root;
}

html_selector* html_selector_from_string (char *str) {
  int i = 0;
  html_selector *first = single_selector (str, &i);
  html_selector *prev = first;
  html_selector *sel;
  while ((sel = single_selector (str, &i))) { prev->next = sel; prev = sel; }
  return first;
}
