#include "array.h"

array* array_alloc (unsigned size) {
  array *a = (array*)malloc (sizeof (array));
  a->size = 0;
  a->_physical_size = size;
  a->buffer = (void**)malloc (a->_physical_size*sizeof(void*));
  return a;
}

void array_free (array *a, int free_elems) {
  if (free_elems) {
    for (int i = 0; i < a->size; ++i) { free (a->buffer [i]); }
  }
  free (a->buffer);
  free (a);
}

void array_free_f (array *a, void(*f)(void*)) {
  if (f) {
    for (int i = 0; i < a->size; ++i) { f (a->buffer [i]); }
  }
  free (a->buffer);
  free (a);
}

void array_realloc (array *a) {
  a->_physical_size *= 2;
  a->buffer = (void*)realloc (a->buffer, a->_physical_size*sizeof(void*));
}

void* array_add (array *a, void *elem) {
  if (elem) {
    if (a->size == a->_physical_size) { array_realloc (a); }
    return (a->buffer [a->size++] = elem);
  } else {
    return 0;
  }
}

void* array_remove (array *a) {
  if (a->size > 0) { return a->buffer [--a->size]; } else { return 0; }
}

void array_clear (array *a, int free_elems) {
  if (free_elems) { for (int i = 0; i < a->size; ++i) { free (a->buffer [i]); }}
  a->size = 0;
}
