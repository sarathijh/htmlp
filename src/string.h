#ifndef STRING_H
#define STRING_H

#include <stdlib.h>

typedef struct {
  unsigned size;
  unsigned _physical_size;
  char *buffer;
} string;

string* string_alloc   (unsigned size);
void    string_free    (string*);
string* string_add     (string*, char c);
string* string_concat  (string*, const char *c);
string* string_remove  (string*);
string* string_clear   (string*);
string* string_tolower (string*);
char*   string_cstr    (string*);

#endif // STRING_H
