#ifndef ITERPARSER_H
#define ITERPARSER_H

#include <stdlib.h>
#include <stdio.h>
#include "array.h"

/**
 * An HTML attribute is a name-value pair.
 */
typedef struct { char *name; char *value; } html_attr;

/**
 * Frees the name and value strings before freeing the attr object itself.
 */
void html_attr_free (html_attr *attr);

/**
 * Parsing event called when an opening tag has been completely parsed.
 * Memory should be freed by the message receiver like so:
 *   free (name);
 *   array_free_f (attrs, FREE_FUNC(html_attr_free));
 */
void (*htmlp_on_tag_open) (char *name, array *attrs);

/**
 * Parsing event called when a closing tag has been completely parsed.
 * Memory should be freed by the message receiver like so:
 *   free (name);
 */
void (*htmlp_on_tag_close) (char *name);

/**
 * Parsing event called when text content between tags is parsed.
 * Memory should be freed by the message receiver like so:
 *   free (text);
 */
void (*htmlp_on_text) (char *text);

/**
 * Parse an html file stream and send messages as parsing events occur.
 */
void htmlp_iterparse (FILE *file);

#endif // ITERPARSER_H
