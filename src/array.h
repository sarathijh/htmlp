#ifndef ARRAY_H
#define ARRAY_H

#include <stdlib.h>

#define FREE_FUNC(f) ((void(*)(void*))f)
#define FOREACH(arr, type, var) \
  type var; for (int i = 0; i < arr->size && (var = (type)arr->buffer [i]); ++i)

typedef struct {
  unsigned size;
  unsigned _physical_size;
  void **buffer;
} array;

array* array_alloc   (unsigned size);
void   array_free    (array*, int free_elems);
void   array_free_f  (array*, void(*)(void*));
void*  array_add     (array*, void *elem);
void*  array_remove  (array*);
void   array_clear   (array*, int free_elems);

#endif // ARRAY_H
