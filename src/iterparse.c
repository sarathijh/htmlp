#include "iterparse.h"
#include "string.h"
#include <stdio.h>
#include <ctype.h>

void html_attr_free (html_attr *attr) {
  free (attr->name);
  free (attr->value);
  free (attr);
}

void has_lt   (FILE*);
void has_text (FILE*);

int issep (int c) { return c == '/' || isspace (c); }

void has_lt (FILE *file) {
  int c = fgetc (file);
  // An html tag starts with '<[a-zA-Z]'.
  if (isalpha (c)) {
    string *tag_name = string_alloc (7);
    array *attrs = array_alloc (5);
    
    // Read the tag name, including the first alpha character already read.
    // Grab all characters until we hit a '>' or a separator.
    do {
      string_add (tag_name, c);
      c = fgetc (file);
    } while (c != '>' && !issep (c));
    
    // Read the tag attributes.
    while (1) {
      // Drop any separator characters.
      while (issep (c)) { c = fgetc (file); }
      // If we've hit a '>', then the tag is complete.
      if (c == '>') { break; }
      
      // Allocate an attribute, and strings for the name and value.
      html_attr *attr = (html_attr*)malloc (sizeof (html_attr));
      string *attr_name = string_alloc (15);
      string *attr_value = string_alloc (15);
      
      // Read the attribute name, including the character already read by the
      // previous step.
      // Grab all characters until we hit an '=' or a separator.
      do {
        string_add (attr_name, c);
        c = fgetc (file);
      } while (c != '=' && !issep (c));
      
      // Drop any separator characters.
      while (issep (c)) { c = fgetc (file); }
      
      // If we found an '=', then look for an attribute value.
      if (c == '=') {
        // Drop any separator characters.
        c = fgetc (file);
        while (issep (c)) { c = fgetc (file); }
        
        if (c == '"') {
          // If we found a '"', then read all characters up until the end '"'.
          c = fgetc (file);
          while (c != '"') { string_add (attr_value, c); c = fgetc (file); }
          c = fgetc (file); // drop the end '"'.
        } else {
          // If there was no '"', then read a single word up until a separator.
          do {
            string_add (attr_value, c);
            c = fgetc (file);
          } while (c != '>' && !issep (c));
        }
      }
      
      // Add the name and value to the attribute.
      attr->name = string_cstr (string_tolower (attr_name));
      attr->value = string_cstr (attr_value);
      
      free (attr_name); // free string container; keep buffer
      free (attr_value); // free string container; keep buffer
      
      // Add the attribute to the array of attributes for this tag.
      array_add (attrs, attr);
    }
    
    // Send the OnTagOpen message.
    htmlp_on_tag_open (string_cstr (string_tolower (tag_name)), attrs);
    free (tag_name);
  }
  // A closing tag starts with '</'
  else if (c == '/') {
    string *tag_name = string_alloc (7);
    
    // Read the tag name.
    // Read characters until we hit a space or a '>'.
    c = fgetc (file);
    while (c != '>' && !isspace (c)) {
      string_add (tag_name, c);
      c = fgetc (file);
    }
    
    // Drop any space characters
    while (isspace (c)) { c = fgetc (file); }
    if (c == '>') {
      // If we hit the ending '>', then send the OnTagClose message.
      htmlp_on_tag_close (string_cstr (tag_name));
    } else {
      // MALFORMATTED
    }
    
    free (tag_name); // free string container; keep buffer
  }
  // If the '<' wasn't immediately followed by an alpha character or '/', then
  // read it as plain text.
  else {
    ungetc (c, file);
    has_text (file);
  }
}

void has_text (FILE *file) {
  string *text = string_alloc (15);
  
  int c = fgetc (file);
  // Keeps grabbing text until we hit an EOF or a '<'.
  // If we hit a '<', then check if it belongs to an html tag; if it does, then
  // stop reading text, otherwise grab the '<' and keep going.
  while (1) {
    while (c != '<' && c != EOF) {
      if (isspace (c)) {
        // If we hit a whitespace character, then add a single space to the
        // output, and drop the rest of the whitespace.
        string_add (text, ' ');
        while (isspace (c)) { c = fgetc (file); }
      } else {
        // Otherwise just add the character to the output.
        string_add (text, c);
        c = fgetc (file);
      }
    }
    if (c == EOF) { break; }
    if (c == '<') {
      c = fgetc (file);
      if (isalpha (c) || c == '/') {
        ungetc (c, file);
        ungetc ('<', file);
        break;
      }
    }
    string_add (text, '<');
  }
  
  // Send the OnText message.
  htmlp_on_text (string_cstr (text));
  free (text); // free string container; keep buffer
}

void htmlp_iterparse (FILE *file) {
  int c = fgetc (file);
  while (c != EOF) {
    if (c == '<') {
      // If we have a '<' then try to read an html tag.
      has_lt (file);
    } else {
      // Otherwise, read it as text.
      ungetc (c, file);
      has_text (file);
    }
    c = fgetc (file);
  }
}
