#ifndef TREE_PARSE_H
#define TREE_PARSE_H

#include "array.h"
#include <stdio.h>

typedef enum { HTML_NODE, TEXT_NODE } html_node_type;

struct html_node {
  html_node_type type;
  char *name;
  char *id;
  char *class_name;
  array *attrs;
  array *children;
  array *child_nodes;
  struct html_node *parent;
  int singleton;
};
typedef struct html_node html_node;

typedef struct {
  html_node_type type;
  char *text;
  struct html_node *parent;
} text_node;

html_node*  html_node_alloc              (char *name, array *attrs);
text_node*  text_node_alloc              (char *text);
void        html_node_free               (html_node*);
html_node*  html_node_append_child       (html_node*, html_node *child);
html_node*  html_node_append_text        (html_node*, text_node *text);
html_node*  html_node_query_selector     (html_node*, char *selector);
array*      html_node_query_selector_all (html_node*, char *selector);

html_node*  htmlp_treeparse (FILE*);
void htmlp_pretty_print (html_node*);

#endif // TREE_PARSE_H
