#include "string.h"
#include <string.h>
#include <ctype.h>

string* string_alloc (unsigned size) {
  string *s = (string*)malloc (sizeof (string));
  s->size = 0;
  s->_physical_size = size;
  s->buffer = (char*)malloc (s->_physical_size*sizeof(char));
  return s;
}

void string_free (string *s) { free (s->buffer); free (s); }

void string_realloc (string *s, unsigned increase) {
  s->_physical_size += increase;
  s->_physical_size *= 2;
  s->buffer = (char*)realloc (s->buffer, s->_physical_size*sizeof(char));
}

string* string_add (string *s, char c) {
  if (s->size == s->_physical_size) { string_realloc (s, 1); }
  s->buffer [s->size++] = c;
  return s;
}

string* string_concat (string *s, const char *c) {
  int n = strlen (c);
  if (s->size + n >= s->_physical_size) { string_realloc (s, n); }
  strncpy (s->buffer + s->size, c, n);
  s->size += n;
  return s;
}

string* string_remove (string *s) {
  if (s->size > 0) { --s->size; }
  return s;
}

string* string_clear (string *s) {
  s->size = 0;
  return s;
}

string* string_tolower (string *s) {
  for (int i = 0; i < s->size; ++i) { s->buffer [i] = tolower (s->buffer [i]); }
  return s;
}

char* string_cstr (string *s) {
  string_add (s, '\0');
  --s->size;
  return s->buffer;
}
