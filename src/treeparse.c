#include "treeparse.h"
#include "iterparse.h"
#include "selector.h"
#include <string.h>

html_node* html_node_alloc (char *name, array *attrs) {
  html_node *node = (html_node*)malloc (sizeof(html_node));
  node->type = HTML_NODE;
  node->name = name;
  node->attrs = attrs;
  node->id = 0;
  node->class_name = 0;
  FOREACH(attrs, html_attr*, attr) {
    if (strcmp (attr->name, "id") == 0) {
      node->id = attr->value;
    } else if (strcmp (attr->name, "class") == 0) {
      node->class_name = attr->value;
    }
  }
  node->children = array_alloc (1);
  node->child_nodes = array_alloc (1);
  node->parent = 0;
  return node;
}

text_node* text_node_alloc (char *text) {
  text_node *node = (text_node*)malloc (sizeof(text_node));
  node->type = TEXT_NODE;
  node->text = text;
  return node;
}

void html_node_free (html_node *node) {
  free (node->name);
  array_free_f (node->attrs, FREE_FUNC(html_attr_free));
  array_free_f (node->children, FREE_FUNC(html_node_free));
  // TODO: Add custom free loop for child_nodes
  free (node);
}

html_node* html_node_append_child (html_node *node, html_node *child) {
  array_add (node->children, child);
  array_add (node->child_nodes, child);
  return (child->parent = node);
}

html_node* html_node_append_text (html_node *node, text_node *text) {
  array_add (node->child_nodes, text);
  return (text->parent = node);
}

html_node* _query_selector (html_node *node, html_selector *sel, array *list) {
  if (sel) {
    if (sel->pattern == 0 || strcmp (node->name, sel->pattern) == 0) {
      int match = 1;
      FOREACH(sel->attrs, html_selector*, attr) {
        if (attr->type == SEL_ID && (!node->id || strcmp (node->id, attr->pattern) != 0)) {
          match = 0;
          break;
        }
      }
      if (match && !(sel = sel->next)) {
        if (list) {
          array_add (list, node);
        } else {
          return node;
        }
      }
    }
    FOREACH(node->children, html_node*, child) {
      html_node *res = _query_selector (child, sel, list);
      if (!list && res) { return res; }
    }
  }
  return 0;
}

html_node* html_node_query_selector (html_node *node, char *selector) {
  return _query_selector (node, html_selector_from_string (selector), 0);
}

array* html_node_query_selector_all (html_node *node, char *selector) {
  array *list = array_alloc (1);
  _query_selector (node, html_selector_from_string (selector), list);
  return list;
}

array *node_stack;
html_node *root_node;

void handle_tag_open (char *name, array *attrs) {
  // TODO: Pull out special attributes like id and class
  html_node *node = html_node_alloc (name, attrs);
  if (node_stack->size > 0) {
    html_node_append_child (node_stack->buffer [node_stack->size-1], node);
  }
  
  static const char *singletons[] = {
    "area", "base", "br", "col", "command", "embed", "hr", "img", "input", 
    "keygen", "link", "meta", "param", "source", "track", "wbr"
  };
  static const int n = sizeof(singletons) / sizeof(char*);
  
  int isSingleton = 0;
  for (int i = 0; i < n; ++i) {
    if ((isSingleton = strcmp (name, singletons [i]) == 0)) { break; }
  }
  node->singleton = isSingleton;
  
  if (!isSingleton) { array_add (node_stack, node); }
  
  if (!root_node) { root_node = node; }
}

void handle_tag_close (char *name) {
  while (node_stack->size > 0 && (strcmp (((html_node*)(node_stack->buffer [node_stack->size-1]))->name, name) != 0)) {
    array_remove (node_stack);
  }
  if (node_stack->size > 0) { array_remove (node_stack); }
}

void handle_text (char *text) {
  if (text [0] == ' ' && text [1] == '\0') {
    free (text);
    return;
  }
  text_node *node = text_node_alloc (text);
  if (node_stack->size > 0) {
    html_node_append_text (node_stack->buffer [node_stack->size-1], node);
  }
}

html_node* htmlp_treeparse (FILE *file) {
  htmlp_on_tag_open  = handle_tag_open;
  htmlp_on_tag_close = handle_tag_close;
  htmlp_on_text      = handle_text;
  
  node_stack = array_alloc (5);
  root_node = 0;
  
  htmlp_iterparse (file);
  
  return root_node;
}

void print_node (html_node *node, int indent) {
  for (int i = 0; i < indent; ++i) { printf ("  "); }
  printf ("<\033[1;1m%s\033[0m", node->name);
  FOREACH(node->attrs, html_attr*, attr) {
    printf (" \033[1;1m%s\033[0m=\"\033[1;33m%s\033[0m\"", attr->name, attr->value);
  }
  printf (">");
  
  if (node->child_nodes->size > 0) {
    if (node->children->size > 0) {
      printf ("\n");
    }
    FOREACH(node->child_nodes, html_node*, child) {
      if (child->type == TEXT_NODE) {
        if (node->children->size > 0) {
          for (int i = 0; i < indent+1; ++i) { printf ("  "); }
        }
        char *text = ((text_node*)child)->text;
        printf ("\033[34;1m%s\033[0m", text [0] == ' ' ? text+1 : text);
        if (node->children->size > 0) {
          printf ("\n");
        }
      } else {
        print_node (child, indent+1);
      }
    }
    if (node->children->size > 0) {
      for (int i = 0; i < indent; ++i) { printf ("  "); }
    }
  }
  
  if (!node->singleton) {
    printf ("</\033[1;1m%s\033[0m>", node->name);
  }
  printf ("\n");
}

void htmlp_pretty_print (html_node *root) {
  if (root) { print_node (root, 0); }
}
