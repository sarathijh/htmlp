#include "common.h"
#include <stdlib.h>
#include <string.h>

char* strdup (const char *orig) {
  size_t n = strlen (orig);
  char *str = (char*)malloc (n+1);
  strncpy (str, orig, n);
  str [n] = '\0';
  return str;
}
