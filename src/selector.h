#ifndef SELECTOR_H
#define SELECTOR_H

#include "treeparse.h"
#include "array.h"

typedef enum { SEL_ID, SEL_CLASS, SEL_TAG } html_selector_type;

struct html_selector {
  html_selector_type type;
  char *pattern;
  array *attrs;
  struct html_selector *next;
};
typedef struct html_selector html_selector;

html_selector* html_selector_from_string (char*);
html_node* html_selector_first_match (html_selector*, html_node*);

#endif // SELECTOR_H
